#include <iostream>

class IntStack {
public:
    IntStack() {
        std::cout << "Enter size of stack: ";
        std::cin >> size;

        _internal = new int *[size];

        std::cout << "Add elements: " << std::endl;

        for (int i = 0; i < size; i++) {
            int elem;
            std::cin >> elem;

            push(elem);
        }

        std::cout << "Done." << std::endl;
    }

    void pop() {
        if (current < 0) {
            std::cout << "Stack is empty." << std::endl;
            return;
        }

        int elem = *_internal[current];
        std::cout << "pop: " << elem << std::endl;
        delete _internal[current];
        _internal[current--] = nullptr;
    }

    void push(int elem) {
        if (current >= size) {
            std::cout << "Stack is full." << std::endl;
            return;
        }

        _internal[++current] = new int(elem);
    }

private:
    int **_internal;
    int size = 0;
    int current = -1;
};

template<typename T>
class Stack {
public:
    Stack() {
        std::cout << "Enter size of stack: ";
        std::cin >> size;

        _internal = new T *[size];

        std::cout << "Add elements: " << std::endl;

        for (int i = 0; i < size; i++) {
            T elem;
            std::cin >> elem;

            push(elem);
        }

        std::cout << "Done." << std::endl;
    }

    void pop() {
        if (current < 0) {
            std::cout << "Stack is empty." << std::endl;
            return;
        }

        auto elem = *_internal[current];
        std::cout << "pop: " << elem << std::endl;
        delete _internal[current];
        _internal[current--] = nullptr;
    }

    void push(T elem) {
        if (current >= size) {
            std::cout << "Stack is full." << std::endl;
            return;
        }

        _internal[++current] = new T(elem);
    }

private:
    T **_internal;
    int size = 0;
    int current = -1;
};

int main() {
    std::cout << "IntStack working" << std::endl;
    IntStack stack;
    stack.pop();
    stack.push(8);
    stack.pop();
    stack.pop();
    std::cout << "======================" << std::endl;

    std::cout << "String typed stack working" << std::endl;
    Stack<std::string> stringStack;
    stringStack.pop();
    stringStack.push("test");
    stringStack.pop();
    stringStack.pop();
    std::cout << "======================" << std::endl;
    return 0;
}
